package com.jsite.common.security.serializer;

import org.apache.shiro.util.ByteSource;

public class ByteSourceUtils {

    public static ByteSource bytes(byte[] bytes) {
        return new JSiteByteSource(bytes);
    }

    public static ByteSource bytes(String arg0) {
        return new JSiteByteSource(arg0.getBytes());
    }
}
