/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.web;

import com.jsite.common.config.Global;
import com.jsite.common.lang.DateUtils;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.web.BaseController;
import com.jsite.modules.yili.entity.YiliEmployee;
import com.jsite.modules.yili.service.YiliEmployeeService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 伊利员工社保信息表Controller
 * @author liuruijun
 * @version 2020-07-29
 */
@Controller
@RequestMapping(value = "${adminPath}/yili/yiliEmployee")
public class YiliEmployeeController extends BaseController {

	@Autowired
	private YiliEmployeeService yiliEmployeeService;
	
	@ModelAttribute
	public YiliEmployee get(@RequestParam(required=false) String id) {
		YiliEmployee entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = yiliEmployeeService.get(id);
		}
		if (entity == null){
			entity = new YiliEmployee();
		}
		return entity;
	}
	
	@RequiresPermissions("yili:yiliEmployee:view")
	@RequestMapping(value = {"list", ""})
	public String list() {
		return "modules/yili/yiliEmployeeList";
	}
	
	@RequiresPermissions("yili:yiliEmployee:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<YiliEmployee> listData(YiliEmployee yiliEmployee, HttpServletRequest request, HttpServletResponse response) {
		Page<YiliEmployee> page = yiliEmployeeService.findPage(new Page<>(request, response), yiliEmployee);
		return page;
	}

	@RequiresPermissions("yili:yiliEmployee:view")
	@RequestMapping(value = "form")
	public String form(YiliEmployee yiliEmployee, Model model) {
		model.addAttribute("yiliEmployee", yiliEmployee);
		return "modules/yili/yiliEmployeeForm";
	}

	@RequiresPermissions("yili:yiliEmployee:edit")
	@RequestMapping(value = "save")
	@ResponseBody
	public String save(YiliEmployee yiliEmployee) {
		if (StringUtils.isBlank(yiliEmployee.getId())) {
			String code = DateUtils.getDate("yyyyMMdd:") + (yiliEmployeeService.getEmployeeCount()+1);
			yiliEmployee.setCode(code);

			YiliEmployee employee = yiliEmployeeService.getByName(yiliEmployee.getName());
			if (employee != null) {
				return renderResult(Global.FALSE, "当前部门已存在相同名字的人员信息");
			}
		}

//		BigDecimal b;
//		for (YiliSocialSecurityInfo securityInfo : yiliEmployee.getYiliSocialSecurityInfoList()) {
//			b = new BigDecimal(securityInfo.getPaymentAmount());
//			securityInfo.setPaymentAmount(b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
//		}

		yiliEmployeeService.save(yiliEmployee);
		return renderResult(Global.TRUE, "保存伊利员工社保信息成功");
	}
	
	@RequiresPermissions("yili:yiliEmployee:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(YiliEmployee yiliEmployee) {
		yiliEmployeeService.delete(yiliEmployee);
		return renderResult(Global.TRUE, "删除伊利员工社保信息成功");
	}

}